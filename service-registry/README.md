
# Service Registry

This minimalistic project provides service discovery with the magic of Spring Boot and Netflix Eureka. The purpose of 
it is to show how a microservice like micro-user can dynamically be scaled horizontally, by bringing more service 
instances up.


### Run the application

The project is available with maven wrapper so, on the command line, go to the project folder and run the following 
commands:

`./mvnw spring-boot:run `

Eureka dashboard will then be available at http://localhost:8110/.
