
# Micro User

Users microservice for CRUD operations.


### Run the application

The project is available with maven wrapper so, on the command line, go to the project folder and run the following 
commands:

`./mvnw spring-boot:run `

Start service-registry first if you want to see service registration happening (no discovery is implemented). The service boots with some users already available in the database. But, as this is a microservice, no UI is provided. 
So, in order to test some requests to the service API, a tool such as Postman (https://www.getpostman.com) is advisable. 
One may use a command line tool instead, like `curl`. Here are some `curl` examples:


##### Get users:

`curl -i http://localhost:8210/api/v1/user`

Users collection is ordered by lastname and firstname in ascending order. Pagination information is available in the 
response's `Link` header. Default pagination field values:
  
  * page - 0
  * limit - 10
  
##### Get users filtered by country code:

`curl -i http://localhost:8210/api/v1/user?countryCode=UK`

Available filters (they can all be combined in the same request):

  * countryCode
  * firstName
  * lastName

##### Create user:

`curl -iX POST  -H "Content-Type:application/json" -d "{\"firstName\":\"Nick\",\"lastName\":\"Kain\",\"nickname\":\"nkain\",\"email\":\"nickkain@mail.com\",\"password\":\"theresnobetter\",\"countryCode\":\"AU\"}" http://localhost:8210/api/v1/user`

##### Create user with email/nickname already in use:

Just run the previous request again. 

##### Update user:

`curl -iX PUT  -H "Content-Type:application/json -d "{\"firstName\":\"Nick\",\"lastName\":\"Kain\",\"nickname\":\"nkain\",\"email\":\"nkainn@mail.com\",\"password\":\"thereisbetterafterall\",\"countryCode\":\"AU\"}" http://localhost:8210/api/v1/user"`

##### Delete user:

To experiment with this request replace `{uuid}` in the link by the uuid from one of the available users.

`curl -iX DELETE http://localhost:8210/api/v1/user/{uuid}`

All user data (except uuid) will be replaced by the user data specified in the request.


### Notes

This REST API implementation tries to follow the best RESTful guidelines. For instance:
  * endpoints naming - they dfine de location of resources, not actions (for that we have HTTP request methods);
  * choosing of HTTP request methods based on the purpose of each method, according to HTTP protocol spec;
  * responses with proper HTTP response code and body (in some use cases the former is subject of debate);
  * paginated results on collection resources(although the decision to use page number instead of cursor, leading to 
    performance issues on large datasets).
    
Although more cumbersome (and not well fitted to be used in database indexes), I decided to use UUID as user id. They
are more suited to be used in a multi-service ecosystem, are easier to migrate, and internal database ids shouldn't be
exposed. One alternative solution could be using both fields, the id for internal usage like queries and indexes, and 
UUID as the public identifier of the user.

User's password information was neglected in this project. We probably shoudn't be returning it with user's info and, 
off course, no clear text passwords in the database, they should be kept salty.

For the API to be capable of evolute without major constraints regarding backward compatibility, user resource is 
versioned ("/api/v1/user"). By prefixing resource path with "/api" we're defining a scope for those endpoints and 
allowing other scopes to be added, like the case of Server Side Events endpoints available on this service. 

Regarding service discovery, for which we are using Netflix Eureka, there's no client availble to validate discovery, 
only registration can be verified on Eureka's dashboard. Also, for the sake of simplicity this service uses an 
in-memory database (H2), so bringing up more instances of the server (only need to pass port number on boot), won't 
mean all instances are using the same database.

Spring Boot does a lot of magic that help us to get something up and running in just a few minutes. That said, that 
magic I don't like it so much, mostly because I feel I don't have the control of what is happening behind the curtain. 
The same is more or less applicable to Hibernate.

I used Flyway to keep the control of the database schema. It provides a nice way to keep our database queries versioned
and database updated. Finally, I'm using it to begin the service with some users already available.

To notify potential service clients I decided to go with Server Side Events, first of all, for the sake of simplicity, 
and also because I assumed this service would only be a notifier. If 2-way communication would be needed, Webscockets 
would fit better in this case. Another solution would be message queueing, but that solution not only is more complex, 
it less friendly for client(front-end) applications to be consumers.
I'm using a broadcaster that is responsable to manage event groups of emitters and deliver messages to them. 


### Tests

The implemented tests fall too short for the project. Only some basic unit tests were implemented to test  
`UserController`, without proper coverage of possible scenarios, and, as opposed to the tests implemented for 
`UserFilter` features, without the adoption of proper tests naming convention.