package com.tftdias.labs.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tftdias.labs.model.User;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.UUID;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class UserControllerTest {

    private static ObjectMapper mapper;

    private static User user1;
    private static User user2;

    @Autowired
    private MockMvc mvc;

    @BeforeClass
    public static void setup() {
        mapper = new ObjectMapper();

        user1 = new User();
        user1.setUuid(UUID.randomUUID());
        user1.setFirstName("John");
        user1.setLastName("Doe");
        user1.setNickname("jdoe");
        user1.setPassword("greatpassword");
        user1.setEmail("johndoe@mail.com");
        user1.setCountryCode("UK");

        user2 = new User();
        user2.setUuid(UUID.randomUUID());
        user2.setFirstName("Richard");
        user2.setLastName("Roe");
        user2.setNickname("rroe");
        user2.setPassword("anothergreatpassword");
        user2.setEmail("richardroe@mail.com");
        user2.setCountryCode("US");
    }

    @Test
    public void getUser() throws Exception {

        mvc.perform(MockMvcRequestBuilders.get("/user/" + user1.getUuid())
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(mapper.writeValueAsString(user1)));
    }

    @Test
    public void createUser() throws Exception {

        mvc.perform(MockMvcRequestBuilders.post("/user")
                .content(mapper.writeValueAsString(user2))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }

    @Test
    public void updateUser() throws Exception {

        User user = new User(user1);

        user.setPassword("greaterpassword");

        String userJson = mapper.writeValueAsString(user1);

        mvc.perform(MockMvcRequestBuilders.put("/user/" + user.getUuid())
                .content(userJson)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(userJson));
    }

    @Test
    public void deleteUser() throws Exception {

        mvc.perform(MockMvcRequestBuilders.delete("/user/" + user2.getUuid()))
                .andExpect(status().isNoContent());
    }

}
