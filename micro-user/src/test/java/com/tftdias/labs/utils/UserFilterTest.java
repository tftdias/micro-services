package com.tftdias.labs.utils;

import com.tftdias.labs.model.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserFilterTest {

    @Test
    public void userFilter_mapWithAllFilterAttributes_create() {

        Map<String, String> filterAttributes = Map.of(
                "firstName", "John",
                "lastName", "Doe",
                "countryCode", "UK"
        );

        UserFilter filter = new UserFilter(filterAttributes);

        assertThat(filter.getFirstName()).isEqualTo("John");
        assertThat(filter.getLastName()).isEqualTo("Doe");
        assertThat(filter.getCountryCode()).isEqualTo("UK");
    }

    @Test
    public void userFilter_mapWithNonexistentFilterAttributes_createWithAvailableValidFilterAttributes() {

        Map<String, String> filterAttributes = Map.of(
                "firstName", "John",
                "lastName", "Doe",
                "planet", "Earth"
        );

        UserFilter filter = new UserFilter(filterAttributes);

        assertThat(filter.getFirstName()).isEqualTo("John");
        assertThat(filter.getLastName()).isEqualTo("Doe");
        assertThat(filter.getCountryCode()).isNull();
    }

    @Test
    public void apply_filterWithFirstNameAttributeEqualToUserFirstName_match() {

        User user = new User();
        user.setFirstName("John");

        Map<String, String> filterAttributes = Map.of(
                "firstName", "John"
        );

        UserFilter filter = new UserFilter(filterAttributes);

        assertThat(filter.apply(user)).isTrue();
    }

    @Test
    public void apply_filterWithLastNameAttributeNotEqualToUserLastName_notMatch() {

        User user = new User();
        user.setFirstName("John");
        user.setLastName("Doe");

        Map<String, String> filterAttributes = Map.of(
                "firstName", "John",
                "lastName", "Roe"
        );

        UserFilter filter = new UserFilter(filterAttributes);

        assertThat(filter.apply(user)).isFalse();
    }
}
