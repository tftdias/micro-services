package com.tftdias.labs.controller;

import com.tftdias.labs.exception.EmailOrNicknameAlreadyInUseException;
import com.tftdias.labs.exception.UserNotFoundException;
import com.tftdias.labs.model.User;
import com.tftdias.labs.service.UserService;
import com.tftdias.labs.utils.UserFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.URI;
import java.util.List;
import java.util.UUID;


@RestController
@RequestMapping("/api/v1/user")
public class UserController {

    private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public ResponseEntity getUsers(@RequestParam(required = false) String firstName,
                                   @RequestParam(required = false) String lastName,
                                   @RequestParam(required = false) String countryCode,
                                   @RequestParam(required = false, defaultValue = "0") int page,
                                   @RequestParam(required = false, defaultValue = "10") int limit,
                                   HttpServletRequest request,
                                   HttpServletResponse response) {

        logger.debug("Received users collection request");

        UserFilter filter = new UserFilter()
                .withFirstName(firstName)
                .withLastName(lastName)
                .withCountryCode(countryCode);

        List<User> users = userService.getUsers(filter, page, limit);

        // an attempt to implement pagination with link header field by
        // follow RFC5988 (https://developer.github.com/v3/#pagination)
        LinkHeaderBuilder linkHeaderBuilder = new LinkHeaderBuilder(
                request,
                page,
                users.size() == limit,
                page > 0 && users.size() > 0);

        response.addHeader("Link", linkHeaderBuilder.build());

        return ResponseEntity.ok(users);
    }

    @GetMapping("/{uuid}")
    public ResponseEntity getUser(@PathVariable String uuid) {

        logger.debug("Received user request (uuid:{})", uuid);

        User user;

        try {

            user = userService.getUser(UUID.fromString(uuid));

        } catch (UserNotFoundException e) {

            logger.debug("Failed to retrieve user (uuid:{}): user does not exist", uuid);

            return ResponseEntity.notFound().build();

        }

        return ResponseEntity.ok(user);
    }

    @PostMapping()
    public ResponseEntity createUser(@RequestBody User user,
                                     HttpServletRequest request) {

        logger.debug("Received user creation request");

        String uuid;

        try {

            uuid = userService.createUser(user).toString();

        } catch (EmailOrNicknameAlreadyInUseException e) {

            logger.debug("Failed to create user: email and/or nickname already in use.");

            return ResponseEntity.status(HttpStatus.CONFLICT)
                    .body(new MessageWrapper(e.getMessage()));
        }

        URI createdUri = UriComponentsBuilder
                .fromUriString(request.getRequestURL().toString())
                .pathSegment(uuid)
                .build()
                .toUri();

        return ResponseEntity.created(createdUri).build();
    }

    @PutMapping("/{uuid}")
    public ResponseEntity updateUser(@PathVariable String uuid, @RequestBody User user) {

        logger.debug("Received user update request");

        User updatedUser;

        try {

            user.setUuid(UUID.fromString(uuid));

            updatedUser = userService.updateUser(user);

        } catch (UserNotFoundException e) {

            logger.debug("Failed to update user (uuid:{}): user does not exist.", uuid);

            return ResponseEntity.notFound().build();

        } catch (EmailOrNicknameAlreadyInUseException e) {

            logger.debug("Failed to update user (uuid:{}): email and/or nickname already in use");

            return ResponseEntity.status(HttpStatus.CONFLICT)
                    .body(new MessageWrapper(e.getMessage()));
        }

        return ResponseEntity.ok(updatedUser);
    }

    @DeleteMapping("/{uuid}")
    public ResponseEntity deleteUser(@PathVariable String uuid) {

        logger.debug("Received user deletion request");

        try {

            userService.deleteUser(UUID.fromString(uuid));

        } catch (UserNotFoundException e) {

            logger.debug("Failed to delete user (uuid: {}): user does not exist", uuid);

            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.noContent().build();
    }


    class MessageWrapper {

        public String message;

        MessageWrapper(String message) {
            this.message = message;
        }
    }


    private class LinkHeaderBuilder {

        private static final String linkTemplate = "<%s>; rel=%s";

        private UriComponentsBuilder uriBuilder;

        private int page;

        private boolean hasNext;

        private boolean hasPrev;

        private StringBuilder valueBuilder;

        LinkHeaderBuilder(HttpServletRequest request,
                          int page,
                          boolean hasNext,
                          boolean hasPrev) {

            this.page = page;
            this.hasNext = hasNext;
            this.hasPrev = hasPrev;

            this.uriBuilder = UriComponentsBuilder
                    .fromUriString(request.getRequestURL().toString() + "?" + request.getQueryString());

            this.valueBuilder = new StringBuilder();
        }

        String build() {

            if (this.hasNext) {
                appendRelation("next", page + 1);
            }

            if (this.hasPrev) {
                appendRelation("prev", page - 1);
            }

            appendRelation("first", 0);

            return valueBuilder.toString();
        }

        private void appendRelation(String rel, int page) {

            if (this.valueBuilder.length() > 0) {
                this.valueBuilder.append(", ");
            }

            URI uri = uriBuilder.replaceQueryParam("page", page)
                    .build()
                    .toUri();

            String link = String.format(linkTemplate, uri.toString(), rel);

            valueBuilder.append(link);
        }
    }
}
