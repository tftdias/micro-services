package com.tftdias.labs.controller;

import com.tftdias.labs.sse.EventBroadcaster;
import com.tftdias.labs.sse.EventType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import javax.servlet.http.HttpServletResponse;
import java.util.Optional;

@Controller
@RequestMapping("/events")
public class SseController {

    private static final Logger logger = LoggerFactory.getLogger(SseController.class);


    private final EventBroadcaster eventBroadcaster;

    @Autowired
    public SseController(EventBroadcaster eventBroadcaster) {
        this.eventBroadcaster = eventBroadcaster;
    }

    @GetMapping("/{eventTypeName}")
    public SseEmitter getEventHandle(@PathVariable String eventTypeName,
                                    HttpServletResponse response) {

        logger.debug("Received handle request for '{}' events", eventTypeName);

        Optional<EventType> eventType = EventType.withName(eventTypeName);

        response.setHeader("Cache-Control", "no-store");

        return eventType.map(eventBroadcaster::createSseEmitter)
                .orElse(null);

    }
}
