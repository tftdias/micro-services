package com.tftdias.labs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.scheduling.annotation.EnableAsync;


@EnableDiscoveryClient
@SpringBootApplication
@EnableAsync
public class MicroUserApp {

    public static void main(String... args) {
        SpringApplication.run(MicroUserApp.class, args);
    }
}
