package com.tftdias.labs.service;

import com.tftdias.labs.exception.EmailOrNicknameAlreadyInUseException;
import com.tftdias.labs.exception.UserNotFoundException;
import com.tftdias.labs.model.User;
import com.tftdias.labs.repository.UserRepository;
import com.tftdias.labs.sse.EventBroadcaster;
import com.tftdias.labs.sse.EventType;
import com.tftdias.labs.utils.UserFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
@Primary
@Qualifier("simple")
public class JpaUserService implements UserService {

    private final UserRepository repository;

    private final EventBroadcaster eventBroadcaster;

    @Autowired
    public JpaUserService(UserRepository repository, EventBroadcaster eventBroadcaster) {

        this.repository = repository;
        this.eventBroadcaster = eventBroadcaster;
    }

    @Override
    public List<User> getUsers(UserFilter userFilter, int page, int size) {

        return repository.findByFirstNameAndLastNameAndCountryCode(
                userFilter.getFirstName(),
                userFilter.getLastName(),
                userFilter.getCountryCode(),
                PageRequest.of(page, size, Sort.Direction.ASC, "last_name", "first_name")
        );
    }

    @Override
    public User getUser(UUID uuid) throws UserNotFoundException {

        User user = repository.findById(uuid)
                .orElseThrow(UserNotFoundException::new);

        return user;
    }

    @Override
    public UUID createUser(User user) throws EmailOrNicknameAlreadyInUseException {

        int count = repository.countByEmailOrNickname(
                user.getEmail(),
                user.getNickname()
        );

        if (count > 0) {
            throw new EmailOrNicknameAlreadyInUseException();
        }

        // FIXME: conflict may still occur if new user with same email or nickname
        //  is created in between

        User newUser = repository.save(user);

        eventBroadcaster.broadcastEvent(EventType.NEW_USER,
                newUser.getUuid().toString());

        return newUser.getUuid();
    }

    @Override
    public User updateUser(User user) throws UserNotFoundException,
            EmailOrNicknameAlreadyInUseException {

        User savedUser = repository.findById(user.getUuid())
                .orElseThrow(UserNotFoundException::new);

        if (!savedUser.getEmail().equalsIgnoreCase(user.getEmail()) ||
                !savedUser.getNickname().equalsIgnoreCase(user.getNickname())) {

            int count = repository.countByEmailOrNicknameAndUuidNotLike(
                    user.getUuid(),
                    user.getEmail(),
                    user.getNickname());

            if (count > 0) {
                throw new EmailOrNicknameAlreadyInUseException();
            }
        }

        // FIXME: conflict may still occur if new user with same email or nickname
        //  is created in between

        User updatedUser = repository.save(user);

        eventBroadcaster.broadcastEvent(EventType.USER_UPDATE,
                updatedUser.getUuid().toString());

        return updatedUser;
    }

    @Override
    public User deleteUser(UUID uuid) throws UserNotFoundException {

        User user = repository.findById(uuid)
                .orElseThrow(UserNotFoundException::new);

        repository.deleteById(uuid);

        return user;
    }
}
