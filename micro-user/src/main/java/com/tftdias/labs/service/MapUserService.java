package com.tftdias.labs.service;

import com.tftdias.labs.exception.EmailOrNicknameAlreadyInUseException;
import com.tftdias.labs.exception.UserNotFoundException;
import com.tftdias.labs.model.User;
import com.tftdias.labs.utils.UserFilter;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
@Qualifier("in-memory")
public class MapUserService implements UserService {

    private Map<UUID, User> users;

    public MapUserService() {

        this.users = Collections.synchronizedMap(new LinkedHashMap<>());
    }

    @Override
    public List<User> getUsers(UserFilter userFilter, int page, int size) {

        return this.users.values().stream()
                .filter(userFilter::apply)
                .collect(Collectors.toList());
    }

    @Override
    public User getUser(UUID uuid) throws UserNotFoundException {

        validateUUID(uuid);

        User user = users.get(uuid);

        if (user == null) {
            throw new UserNotFoundException();
        }

        return user;
    }

    @Override
    public UUID createUser(User user) throws EmailOrNicknameAlreadyInUseException {

        validateUser(user);

        // guarantee user email is not already in use
        if (getUserByEmail(user.getEmail()).isPresent()) {
            throw new EmailOrNicknameAlreadyInUseException();
        }

        UUID uuid = UUID.randomUUID();

        user.setUuid(uuid);

        this.users.put(uuid, user);

        return uuid;
    }

    @Override
    public User updateUser(User user) throws UserNotFoundException,
            EmailOrNicknameAlreadyInUseException {
        validateUser(user);

        User current = users.get(user.getUuid());

        // we could use Map::replace to verify if a map of user's key already
        // exists, but since we need to verify email's uniqueness, this way
        // avoids the overhead of searching for existing user with same email
        if (current == null) {
            throw new UserNotFoundException();
        }

        // guarantee user email is not being updated with one already in use
        if (!current.getEmail().equals(user.getEmail()) &&
                getUserByEmail(user.getEmail()).isPresent()) {

            throw new EmailOrNicknameAlreadyInUseException();
        }

        this.users.put(user.getUuid(), user);

        return null;
    }

    @Override
    public User deleteUser(UUID uuid) throws UserNotFoundException {

        validateUUID(uuid);

        User user = this.users.remove(uuid);

        if (user == null) {

            throw new UserNotFoundException();
        }

        return user;
    }

    private Optional<User> getUserByEmail(String email) {

        return this.users.values().stream()
                .filter(u -> u.getEmail().equals(email))
                .findFirst();
    }

    private static void validateUUID(UUID uuid) {

        if (uuid == null) {

            throw new IllegalArgumentException("UUID cannot be null");
        }
    }

    private static void validateUser(User user) {
        // TODO
    }
}
