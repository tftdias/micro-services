package com.tftdias.labs.service;

import com.tftdias.labs.exception.EmailOrNicknameAlreadyInUseException;
import com.tftdias.labs.exception.UserNotFoundException;
import com.tftdias.labs.model.User;
import com.tftdias.labs.utils.UserFilter;

import java.util.List;
import java.util.UUID;

public interface UserService {

    List<User> getUsers(UserFilter userFilter, int page, int size);

    User getUser(UUID uuid) throws UserNotFoundException;

    UUID createUser(User user) throws EmailOrNicknameAlreadyInUseException;

    User updateUser(User user) throws UserNotFoundException, EmailOrNicknameAlreadyInUseException;

    User deleteUser(UUID uuid) throws UserNotFoundException;

}
