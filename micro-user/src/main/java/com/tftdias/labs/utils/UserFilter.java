package com.tftdias.labs.utils;

import com.tftdias.labs.model.User;

import java.util.Arrays;
import java.util.Map;

public class UserFilter {

    private String firstName;

    private String lastName;


    private String countryCode;

    public UserFilter() {
    }

    public UserFilter(Map<String, String> filterAttributes) {

        Arrays.stream(this.getClass().getDeclaredFields())
                .forEach(
                        field -> {
                            String value = filterAttributes.get(field.getName());

                            if (value != null && !value.isEmpty()) {
                                try {
                                    field.set(this, value);
                                } catch (IllegalAccessException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {

        if (firstName != null) {
            this.firstName = firstName.toLowerCase();
        }
    }

    public UserFilter withFirstName(String firstName) {

        this.setFirstName(firstName);
        return this;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {

        if (lastName != null) {
            this.lastName = lastName.toLowerCase();
        }
    }

    public UserFilter withLastName(String lastName) {

        this.setLastName(lastName);
        return this;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {

        if (countryCode != null) {
            this.countryCode = countryCode.toLowerCase();
        }
    }

    public UserFilter withCountryCode(String countryCode) {

        this.setCountryCode(countryCode);
        return this;
    }

    public boolean apply(User user) {

        if (this.firstName != null &&
                !this.firstName.equals(user.getFirstName().toLowerCase())) {

            return false;
        }

        if (this.lastName != null &&
                !this.lastName.equals(user.getLastName().toLowerCase())) {
            return false;
        }

        if (this.countryCode != null &&
                !this.countryCode.equals(user.getCountryCode().toLowerCase())) {
            return false;
        }

        return true;
    }
}
