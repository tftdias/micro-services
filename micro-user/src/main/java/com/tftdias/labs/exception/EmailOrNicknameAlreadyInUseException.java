package com.tftdias.labs.exception;

public class EmailOrNicknameAlreadyInUseException extends Exception {

    public EmailOrNicknameAlreadyInUseException() {

        super("Email and/or nickname already in use");
    }

    public EmailOrNicknameAlreadyInUseException(String message) {
        super(message);
    }
}
