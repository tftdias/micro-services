package com.tftdias.labs.sse;

import java.util.Arrays;
import java.util.Optional;

public enum EventType {

    NEW_USER("new-user"),
    USER_UPDATE("user-update");

    private final String name;

    EventType(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public static Optional<EventType> withName(String name) {
        return Arrays.stream(EventType.values())
                .filter(et -> et.name.equalsIgnoreCase(name))
                .findFirst();
    }
}
