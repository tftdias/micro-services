package com.tftdias.labs.sse;

import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;

@Component
public class EventBroadcaster {

    private final Map<EventType, CopyOnWriteArrayList<SseEmitter>> emitters;

    public EventBroadcaster() {
        emitters = new HashMap<>();

        Arrays.stream(EventType.values())
                .forEach(type -> emitters.put(type, new CopyOnWriteArrayList<>()));
    }

    public SseEmitter createSseEmitter(EventType type) {

        SseEmitter emitter = new SseEmitter();

        emitters.get(type).add(emitter);

        Runnable runnable = () -> this.emitters.get(type).remove(emitter);

        emitter.onCompletion(runnable);

        emitter.onTimeout(runnable);

        return emitter;
    }

    @Async
    public void broadcastEvent(EventType type, String data) {

        CopyOnWriteArrayList<SseEmitter> typeEmitters = this.emitters.get(type);

        List<SseEmitter> toRemove = typeEmitters.parallelStream()
                .filter(
                        emitter -> {
                            try {
                                emitter.send(SseEmitter.event()
                                        .name(type.getName())
                                        .data(data));

                                return false;
                            } catch (IOException e) {
                                return true;
                            }
                        }
                ).collect(Collectors.toList());

        typeEmitters.removeAll(toRemove);
    }
}
