package com.tftdias.labs.repository;

import com.tftdias.labs.model.User;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface UserRepository extends PagingAndSortingRepository<User, UUID> {

    @Query(value = "SELECT * " +
                   "FROM user u " +
                   "WHERE " +
                   "(:firstName IS NULL OR u.first_name ILIKE :firstName) " +
                   "AND " +
                   "(:lastName IS NULL OR u.last_name ILIKE :lastName) " +
                   "AND " +
                   "(:countryCode IS NULL OR u.country_code ILIKE :countryCode)",

            countQuery = "SELECT count(u.uuid) FROM user u " +
                    "WHERE " +
                    "(:firstName IS NULL OR u.first_name ILIKE :firstName) " +
                    "AND " +
                    "(:lastName IS NULL OR u.last_name ILIKE :lastName) " +
                    "AND " +
                    "(:countryCode IS NULL OR u.country_code ILIKE :countryCode)",

            nativeQuery = true)
    List<User> findByFirstNameAndLastNameAndCountryCode(
            @Param("firstName") String firstName,
            @Param("lastName") String lastName,
            @Param("countryCode") String countryCode,
            Pageable pageable
    );

    @Query(value = "SELECT count(*) " +
                   "FROM user u " +
                   "WHERE " +
                   "u.uuid <> :uuid " +
                   "AND " +
                   "(u.email ILIKE :email OR u.nickname ILIKE :nickname)",
           nativeQuery = true)
    int countByEmailOrNicknameAndUuidNotLike(
            @Param("uuid") UUID uuid,
            @Param("email") String email,
            @Param("nickname") String nickname
    );

    int countByEmailOrNicknameAndUuidIsNot(
            String email,
            String nickname,
            UUID uuid
    );

    int countByEmailOrNickname(
            String email,
            String nickname
    );
}
