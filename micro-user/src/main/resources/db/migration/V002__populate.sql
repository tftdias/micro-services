INSERT into user(first_name, last_name, nickname, email, password, country_code)
  VALUES ('John', 'Doe', 'jdoe', 'johndoe@mail.com', 'greatpassword', 'UK'),
         ('Richard', 'Roe', 'rroe', 'richardroe@mail.com', 'anotherpassword', 'US'),
         ('Marie', 'Doe', 'mdoe', 'mariedoe@mail.com', 'amazingpassword', 'UK'),
         ('Sarah', 'Smith', 'ssmith', 'sarahsmith@mail.com', 'anotheramaingpassword', 'US'),
         ('Jose', 'Mourinho', 'jmourinho', 'josemourinho@mail.com', 'imthespecialone', 'PT')
;