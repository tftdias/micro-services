CREATE TABLE user (
  uuid          uuid NOT NULL DEFAULT RANDOM_UUID(),
  first_name    VARCHAR(50) NOT NULL,
  last_name     VARCHAR(50) NOT NULL,
  nickname      VARCHAR(50),
  email         VARCHAR(100) NOT NULL UNIQUE,
  password      VARCHAR(50) NOT NULL,
  country_code  CHAR(2),
  CONSTRAINT user_pkey PRIMARY KEY (uuid)
);

CREATE INDEX idx_user_uuid ON user (uuid);
CREATE INDEX idx_user_first_name ON user (first_name);
CREATE INDEX idx_user_last_name ON user (last_name);
CREATE INDEX idx_user_email ON user (email);
CREATE INDEX idx_user_country_code ON user (country_code);
